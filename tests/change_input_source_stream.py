import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import ChangeInputSourceStreamResponseError

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ChangeInputSourceStream(unittest.TestCase):
    def test_lifetime(self):
        stream_id = "6735aa4521fd86d57a94ca68"
        channel_id = 1

        def closure():
            resp = client.change_input_source_stream(stream_id, channel_id)

        self.assertRaises(ChangeInputSourceStreamResponseError, closure)


if __name__ == "__main__":
    unittest.main()

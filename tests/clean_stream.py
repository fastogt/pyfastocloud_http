import unittest

from pyfastocloud_base.streams.config import ProxyConfig, ProxyVodConfig, OutputUri
from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import CleanStreamResponseError


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StartStream(unittest.TestCase):
    def test_proxy_stream(self):
        sid = "6735aa4521fd86d57a94ca68"
        out = OutputUri(0, "https://localhost:1234/master.m3u8")
        config = ProxyConfig(sid, [out])

        def closure():
            resp = client.clean_stream(config)

        self.assertRaises(CleanStreamResponseError, closure)

    def test_proxy_vod(self):
        sid = "6735aa4521fd86d57a94ca68"
        out = OutputUri(12, "https://some.tv/live/secure/chunkli1st.m3u8")
        config = ProxyVodConfig(sid=sid, output=[out])

        def closure():
            resp = client.clean_stream(config)

        self.assertRaises(CleanStreamResponseError, closure)


if __name__ == "__main__":
    unittest.main()

import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class GetContent(unittest.TestCase):
    def test_lifetime(self):
        resp = client.get_content()
        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()

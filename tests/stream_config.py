import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import StreamConfigResponseError


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StartConfig(unittest.TestCase):
    def test_lifetime(self):
        id = "618f8abc0d6058eb9240e240"
        def closure():
            resp = client.stream_config(id=id)

        self.assertRaises(StreamConfigResponseError, closure)


if __name__ == "__main__":
    unittest.main()

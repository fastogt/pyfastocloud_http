import unittest

from pyfastocloud_http.public.exceptions import MountS3BucketResponseError
from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class MountS3Bucket(unittest.TestCase):
    def test_lifetime(self):
        name = "some"
        path = "/home/sasha/folder"
        key = "key"
        secret = "secret"

        def closure():
            resp = client.mount_s3_bucket(name, path, key, secret)

        self.assertRaises(MountS3BucketResponseError, closure)


if __name__ == "__main__":
    unittest.main()

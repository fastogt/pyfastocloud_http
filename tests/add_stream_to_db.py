import unittest

# import bson

from pyfastocloud_base.constants import OutputUri, StreamType

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class AddStreamToDB(unittest.TestCase):
    def test_lifetime(self):
        # id = str(bson.ObjectId())
        type = StreamType.VOD_PROXY
        out = OutputUri(0, "https://localhost:1234/master.m3u8")
        urls = [out]
        resp = client.add_vod_to_db(type=type, output=urls)
        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()

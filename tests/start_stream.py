import unittest

from pyfastocloud_base.streams.config import OutputUri, RelayConfig, VodRelayConfig, StreamLogLevel, InputUri
from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StartStream(unittest.TestCase):
    def test_relay_stream(self):
        id = "6735aa4521fd86d57a94ca68"
        feed = "~/streamer/feedback/6735aa4521fd86d57a94ca68"
        data_directory = "~/streamer/data/6735aa4521fd86d57a94ca68"
        log_level = StreamLogLevel.LOG_LEVEL_INFO
        loop = False
        out = OutputUri(0, "https://localhost:1234/master.m3u8")
        input = InputUri(0, "https://localhost:1234/master.m3u8")
        selected_input = 0
        have_audio = True
        have_video = True
        restart_attempts = 10
        audio_tracks_count = 1
        config = RelayConfig(sid=id, feedback_dir=feed, data_directory=data_directory, log_level=log_level,
                             loop=loop, input=[input], output=[
                                 out], selected_input=selected_input,
                             have_audio=have_audio, have_video=have_video, restart_attempts=restart_attempts,
                             audio_tracks_count=audio_tracks_count)

        resp = client.start_stream(config)

        self.assertEqual(resp, None)

    def test_relay_vod(self):
        id = "6735aa4521fd86d57a94ca68"
        feed = "~/streamer/feedback/6735aa4521fd86d57a94ca68"
        data_directory = "~/streamer/data/6735aa4521fd86d57a94ca68"
        log_level = StreamLogLevel.LOG_LEVEL_INFO
        loop = False
        out = OutputUri(0, "https://localhost:1234/master.m3u8")
        input = InputUri(0, "https://localhost:1234/master.m3u8")
        selected_input = 0
        have_audio = True
        have_video = True
        restart_attempts = 10
        audio_tracks_count = 1
        out = OutputUri(
            id=12, uri="https://some.tv/live/secure/chunkli1st.m3u8")
        config = VodRelayConfig(sid=id, feedback_dir=feed, data_directory=data_directory, log_level=log_level,
                             loop=loop, input=[input], output=[
                                 out], selected_input=selected_input,
                             have_audio=have_audio, have_video=have_video, restart_attempts=restart_attempts,
                             audio_tracks_count=audio_tracks_count)

        resp = client.start_stream(config)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()

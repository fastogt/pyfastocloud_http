import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import StartCachedStreamResponseError


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StartStream(unittest.TestCase):
    def test_proxy_stream(self):
        sid = "6735aa4521fd86d57a94ca68"

        def closure():
            resp = client.start_cached_stream(id=sid)

        self.assertRaises(StartCachedStreamResponseError, closure)


if __name__ == "__main__":
    unittest.main()

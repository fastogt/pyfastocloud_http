import unittest

from pyfastocloud_base.constants import OutputUri

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ProbeOutput(unittest.TestCase):
    def test_lifetime(self):
        url = OutputUri(id=0, uri="https://fls-lb-01.dns-online.network:8443/vod/bigbunny.mp4/index.m3u8")

        resp = client.probe_out_stream(url)

        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()

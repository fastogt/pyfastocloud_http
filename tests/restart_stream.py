import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import RestartStreamResponseError

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class RestartStream(unittest.TestCase):
    def test_lifetime(self):
        id = "6036824ced84ffd2c5e50db0"

        def closure():
            resp = client.restart_stream(id)

        self.assertRaises(RestartStreamResponseError, closure)


if __name__ == "__main__":
    unittest.main()

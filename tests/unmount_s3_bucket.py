import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import UnmountS3BucketResponseError

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class MountS3Bucket(unittest.TestCase):
    def test_lifetime(self):
        path = "/home/sasha/folder"

        def closure():
            resp = client.unmount_s3_bucket(path)

        self.assertRaises(UnmountS3BucketResponseError, closure)


if __name__ == "__main__":
    unittest.main()

import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class GetLiveStreamInDB(unittest.TestCase):
    def test_lifetime(self) -> None:
        resp = client.get_live_stream_in_db(id="id")

        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()

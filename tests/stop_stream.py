import unittest

from pyfastocloud_http.fastocloud import FastoCloud
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.exceptions import StopStreamRequestError, StopStreamResponseError


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StopStream(unittest.TestCase):
    def test_lifetime(self):
        id = "6036824ced84ffd2c5e50db0"
        force = False

        def closure():
            client.stop_stream(id, force)

        self.assertRaises(StopStreamResponseError, closure)


if __name__ == "__main__":
    unittest.main()

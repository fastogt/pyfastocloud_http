About PyFastoCloud HTTP
===============

PyFastoCloud HTTP python files.

Dependencies
========
`setuptools pyfastocloud pyfastocloud_base pyfastogt`

Install
========
`python3 setup.py install`
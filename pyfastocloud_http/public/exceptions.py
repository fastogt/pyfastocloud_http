from http import HTTPStatus


class PyFastoCloudRequestException(Exception):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(message)
        self.code = code


class PyFastoCloudResponseException(Exception):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(message)
        self.code = code


class GetVersionResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetContentResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class StartStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class StartCachedStreamRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class StartCachedStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class StopStreamRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class StopStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class CleanStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class RestartStreamRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class RestartStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class StreamConfigRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class StreamConfigResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class ScanFolderRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class ScanFolderResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class ChangeInputSourceStreamRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class ChangeInputSourceStreamResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class MountS3BucketRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class MountS3BucketResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class UnmountS3BucketRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class UnmountS3BucketResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class InjectMasterInputUrlRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class InjectMasterInputUrlResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class RemoveMasterInputUrlRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class RemoveMasterInputUrlResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class ProbeInputUrlRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class ProbeInputUrlResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class ProbeOutputUrlRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class ProbeOutputUrlResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetStreamStatsRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetStreamStatsResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetStreamLogsRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetStreamLogsResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetStreamPipelineRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetStreamPipelineResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class AddVodToDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class AddVodToDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class RemoveStreamFromDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class RemoveStreamFromDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class EditStreamInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class EditStreamInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetListStreamsInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetListSerialsInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetListVodsInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetVodStreamInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetVodStreamInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetSerialInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetSerialInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetLiveStreamsInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetLiveStreamInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GetLiveStreamInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetEpisodesInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class GetEpisodeInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class GeEpisodeInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class AddSeasonInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class AddSeasonInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)


class AddSerialInDBRequestError(PyFastoCloudRequestException):
    def __init__(self) -> None:
        super().__init__(HTTPStatus.BAD_REQUEST, "check the entered data")


class AddSerialInDBResponseError(PyFastoCloudResponseException):
    def __init__(self, code: int, message: str) -> None:
        super().__init__(code, message)

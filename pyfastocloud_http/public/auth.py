class Auth:
    LOGIN = "login"
    PASSWORD = "password"

    def __init__(self, login: str, password: str) -> None:
        self.login = login
        self.password = password

    def __str__(self) -> str:
        return f"Login: {self.login} Password: {self.password}"

    def to_dict(self) -> dict:
        d = dict()

        d[Auth.LOGIN] = self.login
        d[Auth.PASSWORD] = self.password

        return d

from enum import Enum


class Path(Enum):

    GET_VERSION_PATH = "/server/version"
    GET_CONTENT_PATH = "/server/db/content"

    START_STREAM_PATH = "/media/stream/start"
    START_CACHED_STREAM_PATH = "/media/stream/start_cached"
    STOP_STREAM_PATH = "/media/stream/stop"
    CLEAN_STREAM_PATH = "/media/stream/clean"
    RESTART_STREAM_PATH = "/media/stream/restart"
    STREAM_CONFIG_PATH = "/media/stream/config"
    GET_STREAM_STATS_PATH = "/media/stream/stats"
    GET_STREAM_LOGS_PATH = "/media/stream/logs"
    GET_STREAM_PIPELINE_PATH = "/media/stream/pipeline"

    SCAN_FOLDER_PATH = "/media/folder/scan"

    INJECT_MASTER_INPUT_URL_PATH = "/media/stream/inject_master_source"
    REMOVE_MASTER_INPUT_URL_PATH = "/media/stream/remove_master_source"
    CHANGE_INPUT_SOURCE_PATH = "/media/stream/change_source"

    MOUNT_S3_BUCKET_PATH = "/media/s3bucket/mount"
    UNMOUNT_S3_BUCKET_PATH = "/media/s3bucket/unmount"
    GET_MOUNTED_S3_BUCKETS_PATH = "/media/s3bucket/list"

    PROBE_INPUT_STREAM_URL_PATH = "/media/probe_in"
    PROBE_OUTPUT_STREAM_URL_PATH = "/media/probe_out"

    DB_UPLOAD_FILE_PATH = "/server/db/stream/upload_file"  # !

    DB_STREAM_VOD_ADD_PATH = "/server/db/stream/vod/add"
    DB_REMOVE_STREAM_PATH = "/server/db/stream/remove"
    DB_EDIT_STREAM_PATH = "/server/db/stream/edit"

    DB_GET_LIST_LIVE_STREAM_PATH = "/server/db/stream/live/list"
    DB_GET_LIVE_STREAM = "/server/db/stream/live"

    DB_GET_LIST_SERIALS_PATH = "/server/db/serial/list"
    DB_GET_SERIAL_PATH = "/server/db/serial"

    DB_GET_LIST_VODS_PATH = "/server/db/content"
    DB_GET_VOD_STREAM_PATH = "/server/db/vod"

    DB_GET_LIST_EPISODES_PATH = "/server/db/stream/episode/list"
    DB_GET_EPISODE_PATH = "/server/db/stream/episode"

    DB_SEASON_ADD_PATH = "/server/db/season/add"  # !
    DB_SEASON_REMOVE_PATH = "/server/db/season/remove"  # !
    DB_SEASON_EDIT_PATH = "/server/db/season/edit"  # !
    DB_SEASON_LIST_PATH = "/server/db/season/list"  # !
    DB_SEASON_PATH = "/server/season"  # !

    DB_SERIAL_ADD_PATH = "/server/db/serial/add"  # !
    DB_SERIAL_REMOVE_PATH = "/server/db/serial/remove"  # !
    DB_SERIAL_EDIT_PATH = "/server/db/serial/edit"  # !
    DB_SERIAL_LIST_PATH = "/server/db/serial/list"  # !
    DB_SERIAL_PATH = "/server/db/serial"  # !

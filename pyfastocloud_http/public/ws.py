import abc
import json

from typing import Optional

from pyfastocloud_base.constants import ServiceStatisticInfo, QuitStatusInfo, StreamStatisticInfo
from pyfastocloud.fastocloud_client import Commands
from pyfastocloud.structs.ml.notification_info import MlNotificationInfo


class IWSClient(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def onStreamNotification(self, ml_notif):
        raise NotImplementedError

    @abc.abstractmethod
    def onStreamQuit(self, quit_stream):
        raise NotImplementedError

    @abc.abstractmethod
    def onStreamStatistic(self, stat_stream):
        raise NotImplementedError


class WSMessage:
    __slots__ = ["type", "data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise WSMessageError("bad ws message response")

    def __str__(self) -> str:
        return f"type {self.type} data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class WSMessageError(Exception):
    def __init__(self, message):
        super().__init__(message)


class WSObserver:
    def __init__(self, client: Optional[IWSClient]) -> None:
        self.client = client
        self.streams = dict()

    def process_update(self, wsapp, message: str) -> None:
        raw_msg = json.loads(message)

        try:
            msg = WSMessage(**raw_msg)
        except WSMessageError as err:
            raise err

        if msg.type == Commands.STATISTIC_SERVICE_BROADCAST:
            stat = ServiceStatisticInfo(**msg.data)
            self.statistics = stat
        elif msg.type == Commands.QUIT_STATUS_STREAM_BROADCAST:
            quit_stream = QuitStatusInfo(**msg.data)
            sid = quit_stream.id

            if sid in self.streams:
                del self.streams[sid]

                if self.client:
                    self.client.onStreamQuit(quit_stream)
        elif msg.type == Commands.STATISTIC_STREAM_BROADCAST:
            stat_stream = StreamStatisticInfo(**msg.data)
            sid = stat_stream.id

            self.streams[sid] = stat_stream

            if self.client:
                self.client.onStreamStatistic(stat_stream)
        elif msg.type == Commands.ML_NOTIFICATION_STREAM_BROADCAST:
            ml_notif = MlNotificationInfo() #FIXME: MlNotificationInfo(**msg.data)
            ml_notif.update_entry(msg.data)
            if self.client:
                self.client.onStreamNotification(ml_notif)

from pyfastocloud_http.public.exceptions import (
    AddSerialInDBRequestError,
    AddVodToDBRequestError,
    AddVodToDBResponseError,
    CleanStreamResponseError,
    EditStreamInDBRequestError,
    EditStreamInDBResponseError,
    GetContentResponseError,
    GetEpisodeInDBRequestError,
    GetEpisodesInDBResponseError,
    GetListSerialsInDBResponseError,
    GetListStreamsInDBResponseError,
    GetLiveStreamInDBRequestError,
    GetLiveStreamInDBResponseError,
    GetSerialInDBRequestError,
    GetSerialInDBResponseError,
    GetStreamLogsRequestError,
    GetStreamLogsResponseError,
    GetStreamPipelineRequestError,
    GetStreamPipelineResponseError,
    GetStreamStatsRequestError,
    GetStreamStatsResponseError,
    GetVersionResponseError,
    GetVodStreamInDBRequestError,
    GetVodStreamInDBResponseError,
    InjectMasterInputUrlRequestError,
    InjectMasterInputUrlResponseError,
    MountS3BucketRequestError,
    MountS3BucketResponseError,
    ProbeInputUrlRequestError,
    ProbeInputUrlResponseError,
    ProbeOutputUrlRequestError,
    ProbeOutputUrlResponseError,
    RemoveMasterInputUrlResponseError,
    RemoveStreamFromDBRequestError,
    RemoveStreamFromDBResponseError,
    RestartStreamRequestError,
    RestartStreamResponseError,
    ScanFolderRequestError,
    ScanFolderResponseError,
    StartCachedStreamRequestError,
    StartCachedStreamResponseError,
    StartStreamResponseError,
    StopStreamRequestError,
    StopStreamResponseError,
    StreamConfigRequestError,
    StreamConfigResponseError,
    ChangeInputSourceStreamRequestError,
    ChangeInputSourceStreamResponseError,
    UnmountS3BucketRequestError,
    UnmountS3BucketResponseError,
)


class Version:
    __slots__ = ["version"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"version {self.version}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetVersionResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, Version(**value).to_dict())
            else:
                raise GetVersionResponseError(value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Content:
    __slots__ = ["streams", "vods", "serials", "episodes", "seasons"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"streams {self.streams} vods {self.vods} serials {self.serials} episodes {self.episodes} seasons {self.seasons}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetContentResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, Content(**value).to_dict())
            else:
                raise GetContentResponseError(value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StartStream:
    pass


class StartStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StartStreamResponseError(value["code"], value["message"])

    def __str__(self) -> str:
        return f"data: {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StartCachedStreamRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StartCachedStreamRequestError()

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StartCachedStream:
    pass


class StartCachedStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StartCachedStreamResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data: {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StopStreamRequest:
    __slots__ = ["id", "force"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StopStreamRequestError()

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StopStream:
    pass


class StopStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StopStreamResponseError(value["code"], value["message"])

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RestartStreamRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise RestartStreamRequestError()

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RestartStream:
    pass


class RestartStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise RestartStreamResponseError(
                    value["code"], value["message"])

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class CleanStream:
    pass


class CleanStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise CleanStreamResponseError(value["code"], value["message"])

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ScanFolderRequest:
    __slots__ = ["directory", "extensions"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ScanFolderRequestError()

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ScanFolder:
    __slots__ = ["files"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ScanFolderResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, ScanFolder(files=value).to_dict())
            else:
                raise ScanFolderResponseError(value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {"data": self.data}


class StreamConfigRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise StreamConfigRequestError()

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StreamConfig:
    __slots__ = ["config"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"config {self.config}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StreamConfigResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, StreamConfig(**value).to_dict())
            else:
                raise StreamConfigResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ChangeInputSourceStreamRequest:
    __slots__ = ["id", "channel_id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ChangeInputSourceStreamRequestError()

    def __str__(self) -> str:
        return f"id {self.id} channel_id: {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ChangeInputSourceStream:
    pass


class ChangeInputSourceStreamResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ChangeInputSourceStreamResponseError(
                    value["code"], value["message"]
                )

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class MountS3BucketRequest:
    __slots__ = ["name", "path", "key", "secret"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise MountS3BucketRequestError()

    def __str__(self) -> str:
        return f"name {self.name} path {self.path} key {self.key} secret {self.secret}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class MountS3Bucket:
    pass


class MountS3BucketResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise MountS3BucketResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class UnmountS3BucketRequest:
    __slots__ = ["path"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise UnmountS3BucketRequestError()

    def __str__(self) -> str:
        return f"path {self.path}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class UnmountS3Bucket:
    pass


class UnmountS3BucketResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise UnmountS3BucketResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class InjectMasterInputUrlRequest:
    __slots__ = ["id", "url"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise InjectMasterInputUrlRequestError()

    def __str__(self) -> str:
        return f"id {self.id} url {self.url}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class InjectMasterInputUrl:
    pass


class InjectMasterInputUrlResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise InjectMasterInputUrlResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RemoveMasterInputUrlRequest:
    __slots__ = ["id", "url"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise InjectMasterInputUrlRequestError()

    def __str__(self) -> str:
        return f"id {self.id} url {self.url}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RemoveMasterInputUrl:
    pass


class RemoveMasterInputUrlResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise RemoveMasterInputUrlResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeInputUrlRequest:
    __slots__ = ["url"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ProbeInputUrlRequestError()

    def __str__(self) -> str:
        return f"url {self.url}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeInputUrl:
    __slots__ = ["probe", "info"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"probe {self.probe} url {self.info}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeInputUrlResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ProbeInputUrlResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeOutputUrlRequest:
    __slots__ = ["url"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ProbeOutputUrlRequestError()

    def __str__(self) -> str:
        return f"url {self.url}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeOutputUrl:
    __slots__ = ["probe", "info"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"probe {self.probe} url {self.info}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class ProbeOutputUrlResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise ProbeOutputUrlResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetStreamStatsRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamStatsRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StreamStats:
    pass


class GetStreamStatsResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamStatsResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetStreamLogsRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamLogsRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StreamLogs:
    pass


class GetStreamLogsResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamLogsResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetStreamPipelineRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamPipelineRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class StreamPipeline:
    pass


class GetStreamPipelineResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetStreamPipelineResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class AddVodToDBRequest:
    __optional__ = ["id"]
    __slots__ = __optional__ + ["type", "output"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise AddVodToDBRequestError()

    def __str__(self) -> str:
        # __str__ in OutputUri in base repo
        req = f"id {self.id} type {self.type} output {self.output}"
        return req

    def to_dict(self) -> dict:
        req = {}
        for key in self.__slots__:
            if hasattr(self, key):
                req[key] = getattr(self, key)
        return req


class AddVodToDB:
    pass


class AddVodToDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise AddVodToDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RemoveStreamFromDBRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise RemoveStreamFromDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class RemoveStreamFromDB:
    pass


class RemoveStreamFromDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise RemoveStreamFromDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class EditStreamInDBRequest:
    __slots__ = ["id", "type", "output"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise EditStreamInDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id} type {self.type} output {self.output}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class EditStreamInDB:
    pass


class EditStreamInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise EditStreamInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Streams:
    __slots__ = ["streams"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"streams {self.streams}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetListStreamsInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, Streams(**value).to_dict())
            else:
                raise GetListStreamsInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Serials:
    __slots__ = ["serials"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"streams {self.serials}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetListSerialsInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, Serials(**value).to_dict())
            else:
                raise GetListSerialsInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Vods:
    __slots__ = ["streams", "vods", "serials"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    def __str__(self) -> str:
        return f"streams {self.serials}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetListVodsInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, Vods(**value).to_dict())
            else:
                raise GetListSerialsInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetVodStreamInDBRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetVodStreamInDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class VodStream:
    pass


class GetVodStreamInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetVodStreamInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetSerialInDBRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetSerialInDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Serial:
    pass


class GetSerialInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetSerialInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class LiveStreams:
    pass


class GetLiveStreamsInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetLiveStreamInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetLiveStreamInDBRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetLiveStreamInDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class LiveStream:
    pass  # TODO: Validate LiveStream fields


class GetLiveStreamInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetLiveStreamInDBRequestError()

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Episodes:
    pass  # TODO: Validate episodes fields


class GetEpisodesInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetEpisodesInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class GetEpisodeInDBRequest:
    __slots__ = ["id"]

    def __init__(self, **kwargs) -> None:
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetEpisodeInDBRequestError()

    def __str__(self) -> str:
        return f"id {self.id}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}


class Episode:
    pass  # TODO: Validate episode fields


class GetEpisodeInDBResponse:
    __slots__ = ["data"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise GetEpisodesInDBResponseError(
                    value["code"], value["message"])

    def __str__(self) -> str:
        return f"data {self.data}"

    def to_dict(self) -> dict:
        return {key: getattr(self, key) for key in self.__slots__}

import requests
import websocket

from typing import List, Optional

from pyfastocloud_base.constants import StreamId, InputUri, OutputUri, StreamType

from requests.models import HTTPBasicAuth
from pyfastocloud_http.public.exceptions import (
    AddVodToDBRequestError,
    AddVodToDBResponseError,
    CleanStreamResponseError,
    EditStreamInDBRequestError,
    EditStreamInDBResponseError,
    GetContentResponseError,
    GetEpisodeInDBRequestError,
    GetEpisodesInDBResponseError,
    GetListStreamsInDBResponseError,
    GetListVodsInDBResponseError,
    GetLiveStreamInDBRequestError,
    GetLiveStreamInDBResponseError,
    GetLiveStreamsInDBResponseError,
    GetSerialInDBRequestError,
    GetSerialInDBResponseError,
    GetStreamLogsRequestError,
    GetStreamLogsResponseError,
    GetStreamStatsRequestError,
    GetStreamStatsResponseError,
    GetVersionResponseError,
    GetVodStreamInDBResponseError,
    InjectMasterInputUrlRequestError,
    InjectMasterInputUrlResponseError,
    MountS3BucketRequestError,
    MountS3BucketResponseError,
    ProbeInputUrlRequestError,
    ProbeInputUrlResponseError,
    ProbeOutputUrlResponseError,
    RemoveMasterInputUrlRequestError,
    RemoveMasterInputUrlResponseError,
    RemoveStreamFromDBRequestError,
    RemoveStreamFromDBResponseError,
    RestartStreamRequestError,
    RestartStreamResponseError,
    ScanFolderRequestError,
    ScanFolderResponseError,
    StartCachedStreamRequestError,
    StartCachedStreamResponseError,
    StartStreamResponseError,
    StopStreamRequestError,
    StopStreamResponseError,
    StreamConfigRequestError,
    StreamConfigResponseError,
    ChangeInputSourceStreamRequestError,
    ChangeInputSourceStreamResponseError,
    UnmountS3BucketRequestError,
    UnmountS3BucketResponseError,
)

from pyfastocloud_http.public.http import (
    AddVodToDB,
    AddVodToDBRequest,
    AddVodToDBResponse,
    Content,
    EditStreamInDB,
    EditStreamInDBRequest,
    EditStreamInDBResponse,
    Episode,
    Episodes,
    GetContentResponse,
    GetEpisodeInDBRequest,
    GetEpisodeInDBResponse,
    Episodes,
    GetEpisodesInDBResponse,
    GetStreamStatsResponse,
    RemoveStreamFromDB,
    RemoveStreamFromDBRequest,
    RemoveStreamFromDBResponse,
    Serials,
    GetListSerialsInDBResponse,
    StartCachedStreamRequest,
    Streams,
    GetListStreamsInDBResponse,
    Vods,
    GetListVodsInDBResponse,
    LiveStream,
    GetLiveStreamInDBRequest,
    LiveStreams,
    GetLiveStreamsInDBResponse,
    Serial,
    GetSerialInDBRequest,
    GetSerialInDBResponse,
    StreamLogs,
    GetStreamLogsRequest,
    GetStreamLogsResponse,
    StreamPipeline,
    GetStreamPipelineRequest,
    GetStreamPipelineResponse,
    StreamStats,
    GetStreamStatsRequest,
    GetVersionResponse,
    VodStream,
    GetVodStreamInDBRequest,
    GetVodStreamInDBResponse,
    InjectMasterInputUrl,
    InjectMasterInputUrlRequest,
    InjectMasterInputUrlResponse,
    MountS3Bucket,
    MountS3BucketRequest,
    MountS3BucketResponse,
    ProbeInputUrl,
    ProbeInputUrlRequest,
    ProbeInputUrlResponse,
    ProbeOutputUrl,
    ProbeOutputUrlRequest,
    ProbeOutputUrlResponse,
    RemoveMasterInputUrl,
    RemoveMasterInputUrlRequest,
    RemoveMasterInputUrlResponse,
    ScanFolder,
    ScanFolderRequest,
    ScanFolderResponse,
    StartCachedStream,
    StartCachedStreamResponse,
    StreamConfig,
    StreamConfigResponse,
    StartStream,
    StartStreamResponse,
    StopStream,
    StopStreamRequest,
    StopStreamResponse,
    CleanStream,
    CleanStreamResponse,
    RestartStream,
    RestartStreamRequest,
    RestartStreamResponse,
    StreamConfigRequest,
    ChangeInputSourceStream,
    ChangeInputSourceStreamRequest,
    ChangeInputSourceStreamResponse,
    UnmountS3BucketRequest,
    UnmountS3Bucket,
    UnmountS3BucketResponse,
    Version,
)
from pyfastocloud_base.streams.config import IStreamConfig
from pyfastocloud_http.public.path import Path
from pyfastocloud_http.public.auth import Auth
from pyfastocloud_http.public.ws import WSObserver, IWSClient


class FastoCloud:
    def __init__(
        self,
        endpoint: str,
        auth: Optional[Auth] = None,
        real_ip: Optional[str] = None,
        statistics: Optional[str] = None,
    ) -> None:
        self.endpoint = endpoint
        self.auth = auth
        self.real_ip = real_ip
        self.statistics = statistics

    @staticmethod
    def new_fastocloud(endpoint: str, auth: Auth) -> "FastoCloud":
        """
        Create new fastocloud object without real_ip
        """
        return FastoCloud.new_real_ip_fastocloud(endpoint, auth)

    @staticmethod
    def new_real_ip_fastocloud(
        endpoint: str, auth: Auth, real_ip: Optional[str] = None
    ) -> "FastoCloud":
        """
        Create new fastocloud object with real_ip
        """
        return FastoCloud(endpoint, auth, real_ip)

    def get_version(self) -> Version:  # tested
        """
        Get version
        """
        return self._get_version()

    def get_content(self) -> Content:  # tested
        """
        Get version
        """
        return self._get_content()

    def start_stream(self, config: IStreamConfig) -> StartStream:  # tested
        """
        Start fastocloud stream
        """
        return self._start_stream(config)

    def start_cached_stream(self, id: StreamId) -> StartCachedStream:  # tested
        """
        Start cached fastocloud stream
        """
        return self._start_cached_stream(id)

    def restart_stream(self, id: StreamId) -> RestartStream:  # tested
        """
        Restart fastocloud stream
        """
        return self._restart_stream(id)

    def stop_stream(self, id: StreamId, force: bool) -> StopStream:  # tested
        """
        Stop fastocloud stream
        """
        data = {"id": id, "force": force}
        return self._stop_stream(data)

    def clean_stream(self, config: IStreamConfig) -> CleanStream:  # tested
        """
        Clean fastocloud stream
        """
        return self._clean_stream(config)

    def change_input_source_stream(
        self, stream_id: StreamId, channel_id: int
    ) -> ChangeInputSourceStream:  # tested
        """
        Change fastocloud input source stream
        """
        data = {"id": stream_id, "channel_id": channel_id}
        return self._change_input_source_stream(data)

    def inject_master_input_url(
        self, stream_id: StreamId, url: InputUri
    ) -> InjectMasterInputUrl:  # tested
        """
        Inject master input url
        """
        data = {"id": stream_id, "url": url.to_dict()}
        return self._inject_master_input_url(data)

    def remove_master_input_url(
        self, stream_id: StreamId, url: InputUri
    ) -> RemoveMasterInputUrl:  # tested
        """
        Inject master input url
        """
        data = {"id": stream_id, "url": url.to_dict()}
        return self._remove_master_input_url(data)

    def get_stream_stats(self, id: StreamId) -> StreamStats:  # tested
        """
        Get stream statistics
        """
        return self._get_stream_stats(id)

    def stream_config(self, id: StreamId) -> StreamConfig:  # tested
        """
        Get fastocloud stream config
        """
        return self._stream_config(id)

    def get_stream_logs(self, id: StreamId) -> StreamLogs:  # tested
        """
        Get stream logs
        """
        return self._get_stream_logs(id)

    def get_stream_pipeline(self, id: StreamId) -> StreamPipeline:  # tested
        """
        Get stream pipeline
        """
        return self._get_stream_pipeline(id)

    # tested
    def scan_folder(self, directory: str, extensions: List[str]) -> ScanFolder:
        """
        Scan folder
        """
        data = {"directory": directory, "extensions": extensions}
        return self._scan_folder(data)

    def mount_s3_bucket(
        self, name: str, path: str, key: str, secret: str
    ) -> MountS3Bucket:  # tested
        """
        Mount S3 bucket
        """
        data = {"name": name, "path": path, "key": key, "secret": secret}
        return self._mount_s3_bucket(data)

    def unmount_s3_bucket(self, path: str) -> UnmountS3Bucket:  # tested
        """
        Unmount S3 bucket
        """
        return self._unmount_s3_bucket(path)

    def probe_in_stream(self, url: InputUri) -> ProbeInputUrl:  # tested
        """
        Probe input stream url
        """
        data = {"url": url.to_dict()}
        return self._probe_in_stream(data)

    def probe_out_stream(self, url: OutputUri) -> ProbeOutputUrl:  # tested
        """
        Probe input stream url
        """
        data = {"url": url.to_dict()}
        return self._probe_out_stream(data)

    def add_vod_to_db(
        self, type: StreamType, output: List[OutputUri], id: Optional[StreamId] = None
    ) -> AddVodToDB:  # tested
        """
        Add vod to db
        """
        urls = [out.to_dict() for out in output]
        data = {
            "type": type.value,
            "output": urls,
        }
        if id:
            data["id"] = id
        return self._add_vod_to_db(data)

    def remove_stream_from_db(self, id: StreamId) -> RemoveStreamFromDB:
        """
        Remove stream from local storage by id
        """
        return self._remove_stream_from_db(id)

    def edit_stream_in_db(
        self, id: StreamId, type: StreamType, output: List[OutputUri]
    ) -> EditStreamInDB:
        """
        Editing stream to local storage
        """
        data = {
            "id": id,
            "type": type.value,
            "output": [out.to_dict() for out in output],
        }
        return self._edit_stream_in_db(data)

    def get_list_streams_in_db(self) -> Streams:  # tested
        """
        Get list streams in db
        """
        return self._get_list_streams_in_db()

    def get_list_serials_in_db(self) -> Serials:
        """
        Get list seraials in db
        """
        return self._get_list_serials_in_db()

    def get_serial_in_db(self, id: StreamId) -> Serial:
        """
        Get serial in db
        """
        return self._get_serial_in_db(id)

    def get_list_vods_in_db(self) -> Vods:
        """
        Get list vods in db
        """
        return self._get_list_vods_in_db()

    def get_vod_stream_in_db(self, id: StreamId) -> VodStream:
        """
        Get vod stream from db
        """
        return self._get_vod_stream_in_db(id)

    def get_live_streams_in_db(self) -> LiveStreams:
        """
        Get live streams in db
        """
        return self._get_live_streams_in_db()

    def get_live_stream_in_db(self, id: StreamId) -> LiveStream:
        """
        Get live stream in db
        """
        return self._get_live_stream_in_db(id)

    def get_list_episodes_in_db(self) -> Episodes:
        """
        Get episodes in db
        """
        return self._get_list_episodes_in_db()

    def get_episode_in_db(self, id: StreamId) -> Episode:
        """
        Get episode in db
        """
        return self._get_episode_in_db(id)

    def _get_version(self) -> Version:
        try:
            resp_data = self.__send_get_request(Path.GET_VERSION_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetVersionResponse(**resp_data)
        except GetVersionResponseError as err:
            raise err

        return response.data

    def _get_content(self) -> Content:
        try:
            resp_data = self.__send_get_request(Path.GET_CONTENT_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetContentResponse(**resp_data)
        except GetContentResponseError as err:
            raise err

        return response.data

    def _start_stream(self, config: IStreamConfig) -> StartStream:
        request_body = config.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.START_STREAM_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = StartStreamResponse(**resp_data)
        except StartStreamResponseError as err:
            raise err

        return response.data

    def _start_cached_stream(self, id: StreamId) -> StartCachedStream:
        try:
            request = StartCachedStreamRequest(id=id)
        except StartCachedStreamRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.START_CACHED_STREAM_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = StartCachedStreamResponse(**resp_data)
        except StartCachedStreamResponseError as err:
            raise err

        return response.data

    def _stop_stream(self, data: dict) -> StopStream:
        try:
            request = StopStreamRequest(**data)
        except StopStreamRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.STOP_STREAM_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = StopStreamResponse(**resp_data)
        except StopStreamResponseError as err:
            raise err

        return response.data

    def _clean_stream(self, config: IStreamConfig) -> CleanStream:
        request_body = config.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.CLEAN_STREAM_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = CleanStreamResponse(**resp_data)
        except CleanStreamResponseError as err:
            raise err

        return response.data

    def _restart_stream(self, id: StreamId) -> RestartStream:
        try:
            request = RestartStreamRequest(id=id)
        except RestartStreamRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.RESTART_STREAM_PATH.value,
                data=request_body,
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = RestartStreamResponse(**resp_data)
        except RestartStreamResponseError as err:
            raise err

        return response.data

    def _stream_config(self, id: StreamId) -> StreamConfig:
        try:
            request = StreamConfigRequest(id=id).to_dict()
        except StreamConfigRequestError as err:
            raise err

        request_path = f"{Path.STREAM_CONFIG_PATH.value}/{request['id']}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = StreamConfigResponse(**resp_data)
        except StreamConfigResponseError as err:
            raise err

        return response.data

    def _scan_folder(self, data: dict) -> ScanFolder:
        try:
            request = ScanFolderRequest(**data)
        except ScanFolderRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.SCAN_FOLDER_PATH.value,
                data=request_body,
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = ScanFolderResponse(**resp_data)
        except ScanFolderResponseError as err:
            raise err

        return response.data

    def _change_input_source_stream(self, data: dict) -> ChangeInputSourceStream:
        try:
            request = ChangeInputSourceStreamRequest(**data)
        except ChangeInputSourceStreamRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.CHANGE_INPUT_SOURCE_PATH.value,
                data=request_body,
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = ChangeInputSourceStreamResponse(**resp_data)
        except ChangeInputSourceStreamResponseError as err:
            raise err

        return response.data

    def _mount_s3_bucket(self, data: dict) -> MountS3Bucket:
        try:
            request = MountS3BucketRequest(**data)
        except MountS3BucketRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.MOUNT_S3_BUCKET_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = MountS3BucketResponse(**resp_data)
        except MountS3BucketResponseError as err:
            raise err

        return response.data

    def _unmount_s3_bucket(self, path: str) -> UnmountS3Bucket:
        try:
            request = UnmountS3BucketRequest(path=path)
        except UnmountS3BucketRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.UNMOUNT_S3_BUCKET_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = UnmountS3BucketResponse(**resp_data)
        except UnmountS3BucketResponseError as err:
            raise err

        return response.data

    def _inject_master_input_url(self, data: dict) -> InjectMasterInputUrl:
        try:
            request = InjectMasterInputUrlRequest(**data)
        except InjectMasterInputUrlRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.INJECT_MASTER_INPUT_URL_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = InjectMasterInputUrlResponse(**resp_data)
        except InjectMasterInputUrlResponseError as err:
            raise err

        return response.data

    def _remove_master_input_url(self, data: dict) -> RemoveMasterInputUrl:
        try:
            request = RemoveMasterInputUrlRequest(**data)
        except RemoveMasterInputUrlRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.REMOVE_MASTER_INPUT_URL_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = RemoveMasterInputUrlResponse(**resp_data)
        except RemoveMasterInputUrlResponseError as err:
            raise err

        return response.data

    def _probe_in_stream(self, data: dict) -> ProbeInputUrl:
        try:
            request = ProbeInputUrlRequest(**data)
        except ProbeInputUrlRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.PROBE_INPUT_STREAM_URL_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = ProbeInputUrlResponse(**resp_data)
        except ProbeInputUrlResponseError as err:
            raise err

        return response.data

    def _get_stream_stats(self, id: StreamId) -> StreamStats:
        try:
            request = GetStreamStatsRequest(id=id).to_dict()
        except GetStreamStatsRequestError as err:
            raise err

        request_path = f"{Path.GET_STREAM_STATS_PATH.value}/{request['id']}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetStreamStatsResponse(**resp_data)
        except GetStreamStatsResponseError as err:
            raise err

        return response.data

    def _get_stream_logs(self, id: StreamId) -> StreamLogs:
        try:
            request = GetStreamLogsRequest(id=id).to_dict()
        except GetStreamLogsRequestError as err:
            raise err

        request_path = f"{Path.GET_STREAM_LOGS_PATH.value}/{request['id']}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetStreamLogsResponse(**resp_data)
        except GetStreamLogsResponseError as err:
            raise err

        return response.data

    def _get_stream_pipeline(self, id: StreamId) -> StreamPipeline:
        try:
            request = GetStreamPipelineRequest(id=id).to_dict()
        except GetStreamLogsRequestError as err:
            raise err

        request_path = f"{Path.GET_STREAM_PIPELINE_PATH.value}/{request['id']}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetStreamPipelineResponse(**resp_data)
        except GetStreamLogsResponseError as err:
            raise err

        return response.data

    def _probe_out_stream(self, data: dict) -> ProbeOutputUrl:
        try:
            request = ProbeOutputUrlRequest(**data)
        except ProbeInputUrlRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.PROBE_OUTPUT_STREAM_URL_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = ProbeOutputUrlResponse(**resp_data)
        except ProbeOutputUrlResponseError as err:
            raise err

        return response.data

    def _add_vod_to_db(self, data: dict) -> AddVodToDB:
        try:
            request = AddVodToDBRequest(**data)
        except AddVodToDBRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_post_request(
                Path.DB_STREAM_VOD_ADD_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = AddVodToDBResponse(**resp_data)
        except AddVodToDBResponseError as err:
            raise err

        return response.data

    def _remove_stream_from_db(self, id: StreamId) -> RemoveStreamFromDB:
        try:
            request = RemoveStreamFromDBRequest(id=id)
        except RemoveStreamFromDBRequestError as err:
            raise err

        request_path = f"{Path.DB_REMOVE_STREAM_PATH.value}/{request.id}"

        try:
            resp_data = self.__send_delete_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = RemoveStreamFromDBResponse(**resp_data)
        except RemoveStreamFromDBResponseError as err:
            raise err

        return response.data

    def _edit_stream_in_db(self, data: dict) -> EditStreamInDB:
        try:
            request = EditStreamInDBRequest(**data)
        except EditStreamInDBRequestError as err:
            raise err

        request_body = request.to_dict()

        try:
            resp_data = self.__send_patch_request(
                Path.DB_EDIT_STREAM_PATH.value, data=request_body
            )
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = EditStreamInDBResponse(**resp_data)
        except EditStreamInDBResponseError as err:
            raise err

        return response.data

    def _get_list_streams_in_db(self) -> Streams:
        try:
            resp_data = self.__send_get_request(
                Path.DB_GET_LIST_LIVE_STREAM_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetListStreamsInDBResponse(**resp_data)
        except GetListStreamsInDBResponseError as err:
            raise err

        return response.data

    def _get_list_serials_in_db(self) -> Serials:
        try:
            resp_data = self.__send_get_request(
                Path.DB_GET_LIST_SERIALS_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetListSerialsInDBResponse(**resp_data)
        except GetListStreamsInDBResponseError as err:
            raise err

        return response.data

    def _get_serial_in_db(self, id: StreamId) -> Serial:
        try:
            request = GetSerialInDBRequest(id=id)
        except GetSerialInDBRequestError as err:
            raise err

        request_path = f"{Path.DB_GET_SERIAL_PATH.value}/{request.id}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetSerialInDBResponse(**resp_data)
        except GetSerialInDBResponseError as err:
            raise err

        return response.data

    def _get_list_vods_in_db(self) -> Vods:
        try:
            resp_data = self.__send_get_request(
                Path.DB_GET_LIST_VODS_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetListVodsInDBResponse(**resp_data)
        except GetListVodsInDBResponseError as err:
            raise err

        return response.data

    def _get_vod_stream_in_db(self, id: StreamId) -> VodStream:
        try:
            request = GetVodStreamInDBRequest(id=id).to_dict()
        except StreamConfigRequestError as err:
            raise err

        request_path = f"{Path.STREAM_CONFIG_PATH.value}/{request['id']}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetVodStreamInDBResponse(**resp_data)
        except GetVodStreamInDBResponseError as err:
            raise err

        return response.data

    def _get_live_streams_in_db(self) -> LiveStreams:
        try:
            resp_data = self.__send_get_request(
                Path.DB_GET_LIST_LIVE_STREAM_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetLiveStreamsInDBResponse(**resp_data)
        except GetLiveStreamsInDBResponseError as err:
            raise err

        return response.data

    def _get_live_stream_in_db(self, id: StreamId) -> LiveStream:
        try:
            request = GetLiveStreamInDBRequest(id=id)
        except GetLiveStreamInDBRequestError as err:
            raise err

        request_path = f"{Path.DB_GET_LIVE_STREAM.value}/{request.id}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetLiveStreamsInDBResponse(**resp_data)
        except GetLiveStreamInDBResponseError as err:
            raise err

        return response.data

    def _get_list_episodes_in_db(self) -> Episodes:
        try:
            resp_data = self.__send_get_request(
                Path.DB_GET_LIST_EPISODES_PATH.value)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetEpisodesInDBResponse(**resp_data)
        except GetEpisodesInDBResponseError as err:
            raise err

        return response.data

    def _get_episode_in_db(self, id: StreamId) -> Episode:
        try:
            request = GetEpisodeInDBRequest(id=id)
        except GetEpisodeInDBRequestError as err:
            raise err

        request_path = f"{Path.DB_GET_EPISODE_PATH.value}/{request.id}"

        try:
            resp_data = self.__send_get_request(request_path)
        except (requests.ConnectionError, requests.JSONDecodeError) as err:
            raise err

        try:
            response = GetEpisodeInDBResponse(**resp_data)
        except GetEpisodesInDBResponseError as err:
            raise err

        return response.data

    def __send_get_request(self, path: str, headers: Optional[dict] = None) -> dict:
        auth = (
            None
            if not self.auth
            else HTTPBasicAuth(self.auth.login, self.auth.password)
        )
        url = self.endpoint + path

        try:
            resp = requests.get(url, headers=headers, auth=auth)
        except requests.exceptions.ConnectionError as err:
            raise err

        try:
            resp_data = resp.json()
        except requests.JSONDecodeError as err:
            raise err

        resp.close()

        return resp_data

    def __send_post_request(self, path: str, data: Optional[dict] = None) -> dict:
        headers = {"Content-Type": "application/json"}
        auth = (
            None
            if not self.auth
            else HTTPBasicAuth(self.auth.login, self.auth.password)
        )
        url = self.endpoint + path

        try:
            resp = requests.post(url, headers=headers, json=data, auth=auth)
        except requests.exceptions.ConnectionError as err:
            raise err

        try:
            resp_data = resp.json()
        except requests.JSONDecodeError as err:
            raise err

        resp.close()

        return resp_data

    def __send_patch_request(self, path: str, data: Optional[dict] = None) -> dict:
        headers = {"Content-Type": "application/json"}
        auth = (
            None
            if not self.auth
            else HTTPBasicAuth(self.auth.login, self.auth.password)
        )
        url = self.endpoint + path

        try:
            resp = requests.patch(url, headers=headers, json=data, auth=auth)
        except requests.ConnectionError as err:
            raise err

        try:
            resp_data = resp.json()
        except requests.JSONDecodeError as err:
            raise err

        resp.close()

        return resp_data

    def __send_delete_request(self, path: str, data: Optional[dict] = None) -> dict:
        headers = {"Content-Type": "application/json"}
        auth = (
            None
            if not self.auth
            else HTTPBasicAuth(self.auth.login, self.auth.password)
        )
        url = self.endpoint + path

        try:
            resp = requests.delete(url, headers=headers, json=data, auth=auth)
        except requests.ConnectionError as err:
            raise err

        try:
            resp_data = resp.json()
        except requests.JSONDecodeError as err:
            raise err

        return resp_data

    def ws_run(self, client: Optional[IWSClient]) -> None:
        obs = WSObserver(client=client)

        ws_url = self.__generate_ws_url()
        ws = websocket.WebSocketApp(ws_url, on_message=obs.process_update)

        ws.run_forever()

    def __generate_ws_url(self) -> str:
        url = self.endpoint + "/updates"
        ws_url = "ws" + url[4:]

        return ws_url
